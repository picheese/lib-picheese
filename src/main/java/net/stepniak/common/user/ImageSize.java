package net.stepniak.common.user;

import java.util.HashMap;
import java.util.Map;

public enum ImageSize {
  THUMBNAIL("thumbnail"),
  NORMAL("normal"),
  ORIGINAL("original");

  static Map<String, ImageSize> map;

  static {
    map = new HashMap<String, ImageSize>(ImageSize.values().length);

    for (ImageSize size : ImageSize.values()) {
      map.put(size.getType(), size);
    }
  }

  final String type;

  ImageSize(final String type) {
    this.type = type;
  }

  public static ImageSize getSize(final String type) {
    return map.get(type);
  }

  public String getType() {
    return type;
  }
}
