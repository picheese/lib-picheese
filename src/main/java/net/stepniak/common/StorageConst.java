package net.stepniak.common;

public class StorageConst {

  public final static String IMAGE_DEFAULT_TYPE = "jpeg";
  public final static String IMAGE_DEFAULT_MIME_TYPE = "image/" + IMAGE_DEFAULT_TYPE;
  public final static int[] IMAGE_THUMBNAIL_SIZE = {100, 100};
  public final static int[] IMAGE_NORMAL_SIZE = {300, 300};
  public final static String[] IMAGE_ALLOWED_MIME_TYPE = {"image/jpg", "image/jpeg", "image/png", "image/gif"};
  public final static int IMAGE_ALLOWED_IMAGE_SIZE = 10485760;  //10MB
}
