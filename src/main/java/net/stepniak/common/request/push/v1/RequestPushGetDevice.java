package net.stepniak.common.request.push.v1;

import net.stepniak.common.pojos.push.v1.device.PushDeviceType;

public class RequestPushGetDevice {
  private String deviceId;
  private PushDeviceType deviceType;

  public RequestPushGetDevice(String deviceId, PushDeviceType type) {
    this.deviceId = deviceId;
    this.deviceType = type;
  }

  public RequestPushGetDevice() {
  }

  public String getDeviceId() {
    return deviceId;
  }

  public PushDeviceType getDeviceType() {
    return deviceType;
  }
}
