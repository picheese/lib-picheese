package net.stepniak.common.request.push.v1;

import net.stepniak.common.pojos.push.v1.device.PushDeviceType;

public class RequestPushRegister {
  private String deviceId;

  private PushDeviceType deviceType;

  private int messagesMask;

  private long configureMask;

  public RequestPushRegister(String deviceId, PushDeviceType deviceType, int messagesMask, long configureMask) {
    this.deviceId = deviceId;
    this.deviceType = deviceType;
    this.messagesMask = messagesMask;
    this.configureMask = configureMask;
  }

  public RequestPushRegister() {
  }

  public String getDeviceId() {
    return deviceId;
  }

  public PushDeviceType getDeviceType() {
    return deviceType;
  }

  public int getMessagesMask() {
    return messagesMask;
  }

  public long getConfigureMask() {
    return configureMask;
  }
}
