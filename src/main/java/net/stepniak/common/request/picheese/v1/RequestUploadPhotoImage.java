package net.stepniak.common.request.picheese.v1;

public class RequestUploadPhotoImage {
  private String title;
  private String description;
  private String imgBase64;

  public RequestUploadPhotoImage(String title, String description, String imgBase64) {
    this.title = title;
    this.description = description;
    this.imgBase64 = imgBase64;
  }

  public RequestUploadPhotoImage() {
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getImgBase64() {
    return imgBase64;
  }
}
