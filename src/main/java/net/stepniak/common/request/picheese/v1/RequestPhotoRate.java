package net.stepniak.common.request.picheese.v1;

public class RequestPhotoRate {
  private int rate;

  public RequestPhotoRate(int rate) {
    this.rate = rate;
  }

  public RequestPhotoRate() {
  }

  public int getRate() {
    return rate;
  }
}
