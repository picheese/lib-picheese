package net.stepniak.common.request.picheese.v1;

public class RequestUploadPhotoFromUrl {
  private String title;
  private String description;
  private String url;

  public RequestUploadPhotoFromUrl(String title, String description, String url) {
    this.title = title;
    this.description = description;
    this.url = url;
  }

  public RequestUploadPhotoFromUrl() {
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getUrl() {
    return url;
  }
}
