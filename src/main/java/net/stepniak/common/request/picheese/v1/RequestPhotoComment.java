package net.stepniak.common.request.picheese.v1;

public class RequestPhotoComment {
  private String text;

  public RequestPhotoComment(String text) {
    this.text = text;
  }

  public RequestPhotoComment() {
  }

  public String getText() {
    return text;
  }
}
