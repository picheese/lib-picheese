package net.stepniak.common.pojos.picheese.v1;

import net.stepniak.api.auth.sdk.response.User;
import net.stepniak.common.pojos.Base;
import net.stepniak.common.pojos.storage.ImageUrl;

public class Photo extends Base {
  private Long id;

  private ImageUrl url;

  private PhotoRating rating;

  private User user;

  private String title;

  private String description;

  public Photo() {
    super();
  }

  public Photo(Long id, ImageUrl url, PhotoRating rating, User user, String title, String description) {
    this.id = id;
    this.url = url;
    this.rating = rating;
    this.user = user;
    this.title = title;
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public ImageUrl getUrl() {
    return url;
  }

  public PhotoRating getRating() {
    return rating;
  }

  public User getUser() {
    return user;
  }

  public String getDescription() {
    return description;
  }

  public String getTitle() {
    return title;
  }
}