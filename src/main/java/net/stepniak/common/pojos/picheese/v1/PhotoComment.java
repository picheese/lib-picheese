package net.stepniak.common.pojos.picheese.v1;

import net.stepniak.api.auth.sdk.response.User;
import net.stepniak.common.pojos.Base;

public class PhotoComment extends Base {
  private Long id;

  private User user;

  private String text;

  public PhotoComment() {
    super();
  }

  public PhotoComment(Long id, User user, String text) {
    this.id = id;
    this.user = user;
    this.text = text;
  }

  public Long getId() {
    return id;
  }

  public User getUser() {
    return user;
  }

  public String getText() {
    return text;
  }
}