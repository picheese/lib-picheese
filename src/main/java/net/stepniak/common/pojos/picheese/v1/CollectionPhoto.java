package net.stepniak.common.pojos.picheese.v1;

import net.stepniak.common.pojos.v1.CollectionBase;

import java.util.Collections;
import java.util.List;

public class CollectionPhoto extends CollectionBase {
  private List<Photo> entities;  //required to serialize entities!

  public CollectionPhoto() {
    super();
  }

  public CollectionPhoto(int currentPage, int pageLimit, long totalResults, List<Photo> entities) {
    super(currentPage, pageLimit, totalResults);

    if (entities == null) {
      entities = Collections.emptyList();
    }

    this.entities = entities;
  }

  public List<Photo> getEntities() {
    return entities;
  }
}