package net.stepniak.common.pojos.picheese.v1;

import net.stepniak.common.pojos.v1.CollectionBase;

import java.util.Collections;
import java.util.List;

public class CollectionPhotoComment extends CollectionBase {
  private List<PhotoComment> entities;  //required to serialize entities!

  public CollectionPhotoComment() {
    super();
  }

  public CollectionPhotoComment(int currentPage, int pageLimit, long totalResults, List<PhotoComment> entities) {
    super(currentPage, pageLimit, totalResults);

    if (entities == null) {
      entities = Collections.emptyList();
    }

    this.entities = entities;
  }

  public List<PhotoComment> getEntities() {
    return entities;
  }
}