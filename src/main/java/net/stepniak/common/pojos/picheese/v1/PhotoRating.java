package net.stepniak.common.pojos.picheese.v1;

import net.stepniak.common.pojos.Base;

public class PhotoRating extends Base {

  private int score;

  public PhotoRating() {
    super();
  }

  public PhotoRating(int score) {
    this.score = score;
  }

  public int getScore() {
    return score;
  }
}
