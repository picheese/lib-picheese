package net.stepniak.common.pojos.push.v1;

import net.stepniak.common.pojos.v1.CollectionBase;

import java.util.Collections;
import java.util.List;

public class CollectionDevices extends CollectionBase {
  private List<PushDevice> entities;  //required to serialize entities!

  public CollectionDevices() {
    super();
  }

  public CollectionDevices(int currentPage, int pageLimit, long totalResults, List<PushDevice> entities) {
    super(currentPage, pageLimit, totalResults);

    if (entities == null) {
      entities = Collections.emptyList();
    }

    this.entities = entities;
  }

  public List<PushDevice> getEntities() {
    return entities;
  }
}