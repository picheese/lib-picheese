package net.stepniak.common.pojos.push.v1.device;

import java.util.HashMap;
import java.util.Map;

public enum PushDeviceType {
  ANDROID(0, "android"),
  IOS(1, "ios"),
  WP7(2, "wp7");

  private static Map<Integer, PushDeviceType> map;

  static {
    map = new HashMap<Integer, PushDeviceType>(PushDeviceType.values().length);

    for (PushDeviceType item: PushDeviceType.values()) {
      map.put(item.getType(), item);
    }
  }

  private int type;
  private String name;

  PushDeviceType(int type, String name) {
    this.name = name;
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public int getType() {
    return type;
  }

  public static PushDeviceType get(int type) {
    return map.get(type);
  }
}
