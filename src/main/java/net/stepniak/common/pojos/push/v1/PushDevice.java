package net.stepniak.common.pojos.push.v1;

public class PushDevice extends PushBaseDevice {

  private int messagesMask;

  private long configureMask;

  public PushDevice() {
    super();
  }

  public PushDevice(String id, int type, int messagesMask, long configureMask) {
    super(id, type);
    this.messagesMask = messagesMask;
    this.configureMask = configureMask;
  }

  public int getMessagesMask() {
    return messagesMask;
  }

  public long getConfigureMask() {
    return configureMask;
  }
}
