package net.stepniak.common.pojos.push.v1;

import net.stepniak.common.pojos.Base;

public class PushBaseDevice extends Base {

  private String id;

  private int type;

  public PushBaseDevice() {
    super();
  }

  public PushBaseDevice(String id, int type) {
    this.id = id;
    this.type = type;
  }

  public String getId() {
    return id;
  }

  public int getType() {
    return type;
  }
}
