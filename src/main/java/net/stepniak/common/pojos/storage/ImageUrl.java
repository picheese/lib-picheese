package net.stepniak.common.pojos.storage;

import net.stepniak.common.pojos.Base;

public class ImageUrl extends Base {

  private String thumbnail;
  private String normal;
  private String original;

  public ImageUrl() {
    super();
  }

  public ImageUrl(String thumbnail, String normal, String original) {
    this.thumbnail = thumbnail;
    this.normal = normal;
    this.original = original;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public String getNormal() {
    return normal;
  }

  public String getOriginal() {
    return original;
  }
}
