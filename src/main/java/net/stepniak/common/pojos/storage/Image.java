package net.stepniak.common.pojos.storage;

import net.stepniak.common.pojos.Base;

public class Image extends Base {

  private Long id;
  private byte[] thumbnail;
  private byte[] normal;
  private byte[] original;

  public Image() {
    super();
  }

  public Image(Long id, byte[] thumbnail, byte[] normal, byte[] original) {
    this.id = id;
    this.thumbnail = thumbnail;
    this.normal = normal;
    this.original = original;
  }

  public Long getId() {
    return id;
  }

  public byte[] getThumbnail() {
    return thumbnail;
  }

  public byte[] getNormal() {
    return normal;
  }

  public byte[] getOriginal() {
    return original;
  }
}
