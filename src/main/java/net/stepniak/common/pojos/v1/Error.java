package net.stepniak.common.pojos.v1;

import net.stepniak.common.error.http.ServerBaseException;
import net.stepniak.common.pojos.Base;

public class Error extends Base {

  private String msg;
  private String error;
  private String userMsg;
  private String debug;

  public Error() {
    super();
  }

  public Error(ServerBaseException exception, String userMsg) {
    this.msg = exception.getMessage();
    this.error = exception.getErrorCode();
    this.userMsg = userMsg;

    if (exception.getCause() != null) {
      this.debug = exception.getCause().getMessage();
    }
  }

  public Error(String message, String errorCode, String userMsg, Throwable throwable) {
    this.msg = message;
    this.error = errorCode;
    this.userMsg = userMsg;
    this.debug = throwable.getMessage();
  }

  public String getMsg() {
    return msg;
  }

  public String getError() {
    return error;
  }

  public String getUserMsg() {
    return userMsg;
  }

  public String getDebug() {
    return debug;
  }
}
