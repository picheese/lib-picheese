package net.stepniak.common.pojos.v1;

import net.stepniak.common.pojos.Base;

public class CollectionBase extends Base {
  private int currentPage;
  private int pageLimit;
  private long totalResults;
  private int totalPages;
  private boolean isNext;
  private int nextPage;
  private boolean isPrev;
  private int prevPage;

  public CollectionBase() {
    super();
  }

  public CollectionBase(int currentPage, int pageLimit, long totalResults) {
    this.currentPage = currentPage;
    this.pageLimit = pageLimit;
    this.totalResults = totalResults;
    this.totalPages = (int) Math.ceil((double) totalResults / (double) pageLimit);
    this.isNext = (totalPages > currentPage);
    this.nextPage = (this.isNext) ? (currentPage + 1) : currentPage;
    this.isPrev = (currentPage > 1);
    this.prevPage = (this.isPrev) ? (currentPage - 1) : 0;
  }

  public int getCurrentPage() {
    return currentPage;
  }

  public int getPageLimit() {
    return pageLimit;
  }

  public long getTotalResults() {
    return totalResults;
  }

  public int getTotalPages() {
    return totalPages;
  }

  public boolean getIsNext() {
    return isNext;
  }

  public int getNextPage() {
    return nextPage;
  }

  public boolean getIsPrev() {
    return isPrev;
  }

  public int getPrevPage() {
    return prevPage;
  }
}
