package net.stepniak.common.error.server;

import java.util.HashMap;
import java.util.Map;

public enum ServerErrorType {
  METHOD_NOT_ALLOWED("Method not allowed, try using get/post/put/delete instead", "METHOD_NOT_ALLOWED"),
  BAD_REQUEST("Something wrong with your request.", "BAD_REQUEST"),
  NOT_FOUND("Resource not found", "NOT_FOUND"),
  INTERNAL_EXCEPTION("Internal server error", "INTERNAL_EXCEPTION"),
  HTTPS_REQUIRED("Https is required to call this method", "HTTPS_REQUIRED"),
  UNAUTHORIZED("You are not authorized to call this method", "UNAUTHORIZED"),
  FORBIDDEN("You can not call this method", "FORBIDDEN"),
  UNSUPPORTED_MEDIA_TYPE("Entity of the request is in a format not supported by the requested resource for the requested method", "UNSUPPORTED_MEDIA_TYPE"),;

  static private Map<String, ServerErrorType> lookupByErrorCode;

  static {
    lookupByErrorCode = new HashMap<String, ServerErrorType>(ServerErrorType.values().length);

    for (ServerErrorType item : ServerErrorType.values()) {
      lookupByErrorCode.put(item.getErrorCode(), item);
    }
  }

  private final String msg;
  private final String errorCode;

  ServerErrorType(String msg, String errorCode) {
    this.msg = msg;
    this.errorCode = errorCode;
  }

  public String getMsg() {
    return msg;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public static ServerErrorType create(String errorCode) {
    return lookupByErrorCode.get(errorCode);
  }
}
