package net.stepniak.common.error.http.badRequest;

import net.stepniak.common.error.http.BadRequestException;

public class BadRequestValidateException extends BadRequestException {

  public BadRequestValidateException(String validatorMessage) {
    super(BadRequestType.get(validatorMessage), null);
  }
}
