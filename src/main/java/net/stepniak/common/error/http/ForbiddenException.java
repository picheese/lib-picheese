package net.stepniak.common.error.http;

import net.stepniak.common.error.http.forbidden.ForbiddenType;
import net.stepniak.common.error.server.ServerErrorType;

/**
 * Thrown on 403 Forbidden
 */
public class ForbiddenException extends ServerBaseException {

  public ForbiddenException() {
    this(null);
  }

  public ForbiddenException(Throwable cause) {
    super(ServerErrorType.FORBIDDEN, cause, 403);
  }

  public ForbiddenException(ForbiddenType forbiddenType, Throwable cause) {
    super(forbiddenType.getMessage(), forbiddenType.getErrorCode(), cause, 403);
  }
}