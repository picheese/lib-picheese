package net.stepniak.common.error.http;

import net.stepniak.common.error.http.badRequest.BadRequestType;
import net.stepniak.common.error.server.ServerErrorType;

/**
 * Thrown on 400 Bad Request
 */
public class BadRequestException extends ServerBaseException {

  public BadRequestException(Throwable cause) {
    super(ServerErrorType.BAD_REQUEST, cause, 400);
  }

  public BadRequestException(BadRequestType badRequestType) {
    super(badRequestType.getMessage(), badRequestType.getErrorCode(), null, 400);
  }

  public BadRequestException(BadRequestType badRequestType, Throwable cause) {
    super(badRequestType.getMessage(), badRequestType.getErrorCode(), cause, 400);
  }
}