package net.stepniak.common.error.http;

import net.stepniak.common.error.server.ServerErrorType;

/**
 * Thrown on 405 Method not allowed
 */
public class MethodNotAllowedException extends ServerBaseException {

  public MethodNotAllowedException() {
    this(null);
  }

  public MethodNotAllowedException(Throwable cause) {
    super(ServerErrorType.METHOD_NOT_ALLOWED, cause, 405);
  }
}