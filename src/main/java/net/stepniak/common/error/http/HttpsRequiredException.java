package net.stepniak.common.error.http;

import net.stepniak.common.error.server.ServerErrorType;

public class HttpsRequiredException extends ServerBaseException {

  public HttpsRequiredException() {
    this(null);
  }

  public HttpsRequiredException(Throwable cause) {
    super(ServerErrorType.HTTPS_REQUIRED, cause, 409);
  }
}