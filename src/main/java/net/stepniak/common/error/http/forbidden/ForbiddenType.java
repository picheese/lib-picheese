package net.stepniak.common.error.http.forbidden;

public enum ForbiddenType {

  USER_ALREADY_EXISTS("User already exists", "USER_ALREADY_EXISTS"),
  ALREADY_RATED_PHOTO("Already rated photo", "ALREADY_RATED_PHOTO");

  private final String message;
  private final String errorCode;

  ForbiddenType(String message, String errorCode) {
    this.message = message;
    this.errorCode = errorCode;
  }

  public String getMessage() {
    return message;
  }

  public String getErrorCode() {
    return errorCode;
  }
}