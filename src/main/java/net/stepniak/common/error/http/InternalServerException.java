package net.stepniak.common.error.http;

import net.stepniak.common.error.server.ServerErrorType;

/**
 * Thrown on 500 internal server
 */

public class InternalServerException extends ServerBaseException {

  public InternalServerException() {
    this(null);
  }

  public InternalServerException(Throwable cause) {
    super(ServerErrorType.INTERNAL_EXCEPTION, cause, 500);
  }
}