package net.stepniak.common.error.http;

import net.stepniak.common.error.http.notAuthorized.NotAuthorizedType;
import net.stepniak.common.error.server.ServerErrorType;

/**
 * Thrown on 401 Unauthorized
 */
public class NotAuthorizedException extends ServerBaseException {

  public NotAuthorizedException() {
    this(null);
  }

  public NotAuthorizedException(Throwable cause) {
    super(ServerErrorType.UNAUTHORIZED, cause, 401);
  }

  public NotAuthorizedException(NotAuthorizedType notAuthorizedType, Throwable cause) {
    super(notAuthorizedType.getMessage(), notAuthorizedType.getErrorCode(), cause, 401);
  }
}