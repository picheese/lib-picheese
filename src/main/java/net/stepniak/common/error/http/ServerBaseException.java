package net.stepniak.common.error.http;

import net.stepniak.common.error.server.ServerErrorType;

public abstract class ServerBaseException extends RuntimeException {

  private int status;
  private String errorCode;

  public ServerBaseException(String msg, String errorCode, Throwable cause, int status) {
    super(msg, cause);
    this.status = status;
    this.errorCode = errorCode;
  }

  public ServerBaseException(ServerErrorType serverErrorType, Throwable cause, int status) {
    super(serverErrorType.getMsg(), cause);
    this.status = status;
    this.errorCode = serverErrorType.getErrorCode();
  }

  public int getStatus() {
    return status;
  }

  public String getErrorCode() {
    return errorCode;
  }
}