package net.stepniak.common.error.http;

import net.stepniak.common.error.server.ServerErrorType;

/**
 * Thrown on 415 Unsupported Media Type
 */
public class UnsupportedMediaTypeException extends ServerBaseException {

  public UnsupportedMediaTypeException() {
    this(null);
  }

  public UnsupportedMediaTypeException(Throwable cause) {
    super(ServerErrorType.UNSUPPORTED_MEDIA_TYPE, cause, 415);
  }
}