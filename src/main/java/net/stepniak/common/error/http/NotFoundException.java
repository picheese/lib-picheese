package net.stepniak.common.error.http;

import net.stepniak.common.error.server.ServerErrorType;

/**
 * Thrown on 404 Not Found
 */
public class NotFoundException extends ServerBaseException {

  public NotFoundException() {
    this(null);
  }

  public NotFoundException(Throwable cause) {
    super(ServerErrorType.NOT_FOUND, cause, 404);
  }
}