package net.stepniak.common.error.http.notAuthorized;

public enum NotAuthorizedType {

  INVALID_KEY("Invalid api key", "INVALID_KEY"),
  UNAUTHORIZED_SESSION_ID("Unauthorized sessionId (anonymous user)", "UNAUTHORIZED_SESSION_ID"),
  INVALID_SESSION_ID("Invalid sessionId", "INVALID_SESSION_ID"),
  INVALID_AUTH_FOR_USER("Unable to authorize user by given email and userName", "INVALID_AUTH_FOR_USER");

  private final String message;
  private final String errorCode;

  NotAuthorizedType(String message, String errorCode) {
    this.message = message;
    this.errorCode = errorCode;
  }

  public String getMessage() {
    return message;
  }

  public String getErrorCode() {
    return errorCode;
  }
}