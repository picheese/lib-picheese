package net.stepniak.common.error.http.badRequest;

import net.stepniak.api.auth.sdk.AuthConst;
import net.stepniak.common.Const;
import net.stepniak.common.PicheeseConst;
import net.stepniak.common.user.ImageSize;

import java.util.HashMap;
import java.util.Map;

public enum BadRequestType {

  PARAM_KEY("key", "PARAM_KEY"),
  PARAM_SESSION_ID("sessionId", "PARAM_SESSION_ID"),
  PARAM_IMAGE_BASE64("imgBase64", "PARAM_IMAGE_BASE64"),
  PARAM_URL("url", "PARAM_URL"),
  PARAM_DESCRIPTION("description", "PARAM_DESCRIPTION"),
  PARAM_DESCRIPTION_LENGTH("description", PicheeseConst.PHOTOS_DESCRIPTION_LENGHT, "PARAM_DESCRIPTION_LENGTH"),
  PARAM_COMMENT("text", "PARAM_COMMENT"),
  PARAM_COMMENT_LENGTH("text", PicheeseConst.PHOTOS_COMMENT_LENGHT, "PARAM_COMMENT_LENGTH"),
  PARAM_TITLE("title", "PARAM_TITLE"),
  PARAM_TITLE_LENGTH("title", PicheeseConst.PHOTOS_TITLE_LENGHT, "PARAM_TITLE_LENGTH"),
  PARAM_PAGE_NUMBER("page", "PARAM_PAGE_NUMBER"),
  PARAM_PAGE_LIMIT("limit", Const.MAX_LIMIT_PARAM, "PARAM_PAGE_LIMIT"),
  PARAM_RATE("rate", new String[]{"1", "-1"}, "PARAM_RATE"),
  PARAM_USER_EMAIL("email", "PARAM_USER_EMAIL"),
  PARAM_USER_EMAIL_LENGTH("email", AuthConst.USER_EMAIL_LENGHT, "PARAM_USER_EMAIL_LENGTH"),
  PARAM_USER_NAME("userName", "PARAM_USER_NAME"),
  PARAM_USER_NAME_LENGTH("UserName", AuthConst.USER_NAME_LENGHT, "PARAM_USER_NAME_LENGTH"),
  PARAM_PHOTO_ID("photoId", "PARAM_PHOTO_ID"),
  PARAM_IMAGE_MIME_TYPE("imgBase64", "given mime type is not supported", "IMAGE_MIME_TYPE"),
  PARAM_IMAGE_CONTENT("imgBase64", "something wrong with image", "IMAGE_CONTENT"),
  PARAM_IMAGE_CONTENT_SIZE("imgBase64", "image is too big", "IMAGE_CONTENT_SIZE"),
  PARAM_IMAGE_SIZE("imgBase64", new String[]{ImageSize.THUMBNAIL.getType(), ImageSize.NORMAL.getType(), ImageSize.ORIGINAL.getType()}, "IMAGE_CONTENT_SIZE"),
  PARAM_SERVICE_TYPE("serviceType", "PARAM_SERVICE_TYPE"),
  PARAM_PUSH_DEVICE_ID("deviceId", "PARAM_PUSH_DEVICE_ID"),
  PARAM_PUSH_DEVICE_TYPE("deviceType", "PARAM_PUSH_DEVICE_TYPE"),
  PARAM_PUSH_MESSAGES_MASK("messagesMask", "PARAM_PUSH_MESSAGES_MASK"),;

  static private Map<String, BadRequestType> map = new HashMap<String, BadRequestType>(BadRequestType.values().length);

  static {
    for (BadRequestType type : BadRequestType.values()) {
      map.put(type.errorCode, type);
    }
  }

  private final String errorCode;

  private String message;

  BadRequestType(String field, String errorCode) {
    this.message = String.format("Invalid value for required param: %s", field);
    this.errorCode = errorCode;
  }

  BadRequestType(String field, int maxLength, String errorCode) {
    this(field, errorCode);
    this.message += String.format(" - value too long (max %d chars).", maxLength);
  }

  BadRequestType(String field, String additionalInfo, String errorCode) {
    this(field, errorCode);
    this.message += String.format(" - %s", additionalInfo);
  }

  BadRequestType(String field, String[] allowed, String errorCode) {
    this(field, errorCode);
    this.message += String.format(" - allowed values: %s, %s", allowed[0], allowed[1]);
  }

  public static BadRequestType get(String errorCode) {
    return map.get(errorCode);
  }

  public String getMessage() {
    return message;
  }

  public String getErrorCode() {
    return errorCode;
  }
}