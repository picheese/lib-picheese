package net.stepniak.common.error.server;

import org.junit.Test;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class ServerErrorTypeTest {

  @Test
  public void shouldBeCreated() {
    ServerErrorType serverErrorType = ServerErrorType.NOT_FOUND;

    assertThat(serverErrorType.getErrorCode(), notNullValue());
  }
}